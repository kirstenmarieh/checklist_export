import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Output, Input, State
import pandas as pd

app=dash.Dash(__name__)

app.layout = html.Div([
    html.Div([dcc.Input(id="save-as", value='filename')]),
    html.Div(id='output'),
    html.Button('Export', id='btn1', n_clicks=0),
    dcc.Download(id='download-text'),

    dcc.Checklist( id='checklist',
     options=[
         {'label': 'New York City', 'value': 'NYC'},

         {'label': 'Montréal', 'value': 'MTL'},

         {'label': 'San Francisco', 'value': 'SF'}
     ],
     value=['NYC', 'MTL'],
 ),
    html.Div(id= 'save')
 ])

''''@app.callback(
    Output('output', 'children'),
    Input('save-as','value'),
    prevent_initial_call=True
)

def save_as(val):
    file_name=format(val)
    print(file_name)
    return file_name'''
    #return 'Output: {}', format(val)


'''@app.callback(
    Output('download-text', 'data'),
    Input('btn1', 'n_clicks'),
    Input('checklist', 'value'),
    prevent_initial_call=True,
)'''


@app.callback(
    Output('save', 'children'),
    Input('btn1', 'n_clicks'),
    State('checklist', 'value'),
    State('save-as', 'value'),
    prevent_initial_call=True,
)

def func(n_clicks, value, input_name):
    if n_clicks is None:
        raise dash.exceptions.PreventUpdate
    else: #write values to file
        df = pd.DataFrame(value)
        file_name=str(input_name)+'.csv'
        csv =df.to_csv(file_name)
        return 'Successfully exported as ', format(file_name)

if __name__=="__main__":
    app.run_server(debug=True)

